We provide Osteopathic Manipulative Medicine, Primary Care, Massage, Yoga instruction, and Medication Assisted Opioid Addiction Treatment. The most common complaint we see is any sort of pain, especially persistent pain and functional limitation after injury that hasn't responded to other approaches.

Address: 3955 Eagle Creek Pkwy, Suite A, Indianapolis, IN 46254, USA

Phone: 317-410-9978

Website: http://www.drstarsiak.com
